# Bootstrap Mini-Course Installation



## Getting started
- [ ] First of all, please creaste the following folders in your local directory 
```
cd $HOME
mkdir install 
mkdir hyperion-projects 
mkdir simpleboot 
```
We will install most of the libraries in `$HOME/install` and build `scalars-3d` and `fermions-3d` inside hyperion. 

- [ ] For those without `stack` installed, please follow the instruction here https://docs.haskellstack.org/en/stable/install_and_upgrade/ and be sure to include the directory where the `stack` executables are installed in your `PATH`. This can be done by editing the ~/.bashrc file.

- [ ] To keep your own version of repository, I recommend you **fork** the original repository following the instructions https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html. By folking, you keep a remote copy of your own and keep track of your own projects. Works from forks can be merged into the original repository easily with merge requests. This will be particularly helpful working with `scalars-3d` and `fermions-3d`. 

## SDPB, Scalar-blocks, Blocks-3d Installation 
- [ ] [sdpb](https://github.com/AikeLiu/sdpb/) Please general instructions in [install.md](https://github.com/AikeLiu/sdpb/blob/master/Install.md), and instructions specific to each cluster in the `docs/site_installs`. **But** you will need a modified verision of `elemental`. Please clone this repository
https://gitlab.com/AikeLiu/elemental and switch to the branch MPFR_LogExp. 
```
git clone https://gitlab.com/AikeLiu/elemental.git libelemental
cd libelemental
git checkout MPFR_LogExp
# to make sure that you are on the correct branch
git branch 
# this should return
* MPFR_LogExp
  master
# Then build the library following 
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/install -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc
```
Then follow the rest of instructions in the `docs/site_installs`. In particular, make sure to install the following version of the formatting library `fmt` as given in the instruction. 
```
wget https://github.com/fmtlib/fmt/releases/download/6.2.1/fmt-6.2.1.zip
mkdir build
cd build
cmake -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$HOME/install ..
```
The newest fmt library is not compatible with current version of fermions-3d. 

To compile the skydiving executable, you need to switch to a different branch. 
```
git checkout skydiving-release 
./waf 
```

- [ ] [scalar-blocks](https://gitlab.com/bootstrapcollaboration/scalar_blocks) Return to your home directory `$HOME` and follow the instructions in Install.md. 
- [ ] [blocks-3d](https://gitlab.com/bootstrapcollaboration/blocks_3d) Return to your home directory `$HOME` and  follow the instructions in Install.md. 

## Hyperion Installation 
Go to the directory `$HOME/hyperion-projects` 
```
git clone https://gitlab.com/davidsd/scalars-3d.git
git clone https://gitlab.com/davidsd/fermions-3d.git
``` 
then `cd` into `scalars-3d` and `fermions-3d` respectively and follow the instructions in Readme.md. The steps are almost the same so I copied them here too. 
- [ ] [scalars-3d](https://gitlab.com/davidsd/scalars-3d) 
- [ ] [fermions-3d](https://gitlab.com/davidsd/fermions-3d)  

```
cp -r hyperion-config/caltech-hpc-dsd hyperion-config/your-cluster-your-name
```
Edit 'hyperion-config/your-cluster-your-name/Config.hs' as necessary. 

Edit the scripts in 'hyperion-config/your-cluster-your-name' to include the paths to the executables available on your cluster. 
```
mkdir hyperion-config/src
cd hyperion-config/src
ln -s ../your-cluster-your-name/Config.hs Config.hs

```
After finishing steps above, try to compile the code using the stack toolbox 
```
stack build 
stack install 
```
Finally, in `scalars-3d`  try calling 
```
./hyp IsingSigEpsAllowed_test_nmax6
```
and in `fermions-3d` 
```
./hyp FourFermions3dAllowed_test_nmax6
```
## Simple Boot Installation 

- [ ] [Set up project integrations](https://gitlab.com/AikeLiu/haskell-crash-course/-/settings/integrations)


