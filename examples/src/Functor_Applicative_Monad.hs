module Functor_Applicative_Monad where

------------------
-- Functor Class-- 
------------------

-- Built-in map on list, type the following in the interpreter 
-- :t map 
-- map (+1) [1,2,3]
-- fmap (+1) [1,2,3]

-- Define a function to increment each element of a list
incrementList :: Functor f => f Int -> f Int
incrementList list = fmap (+1) list
--incrementList = fmap (+1)
--incrementList list = (+1) <$> list

exampleList = [1, 2, 3]
incremented = incrementList exampleList

-- Maybe Type, type the following in the interpreter
-- fmap (+1) (Just 3)
-- fmap (+1) Nothing



----------------------
-- Applicative Class-- 
----------------------

-- Type the following in the interpreter 
-- pure 1 :: [Int]
-- [(+1),(*100),(*5)] <*> [1,2,3]
-- pure 1 :: Maybe Int
-- Just (+3) <*> Just 1
-- Nothing <*> Just 1
-- Just (+3) <*> Nothing

add :: Int -> Int -> Int
add x y = x + y

xs :: [Int]
xs = [1, 2, 3]

ys :: [Int]
ys = [10, 20, 30]

-- Using the Applicative instance of lists, we can apply the add function
-- to corresponding elements of xs and ys to get a list of sums
zs :: [Int]
zs = add <$> xs <*> ys


-----------------
-- Monad Class -- 
-----------------

half :: Int -> Maybe Int 
half x | even x = Just (x `div` 2 :: Int)
       | otherwise  = Nothing

-- type the following in the interpreter 
-- :t (/)
-- :t (div)
-- :t half
-- Just (20) >>= half >>= half 
halfdo :: Maybe Int
halfdo = do
  x <- Just 20
  y <- half x
  z <- half y
  w <- half z
  return w

-- [1,2,3] >>= (\x -> [4,5] >>= (\y -> return (x,y)))

-- Just 3 >>= (\x -> Just "!" >>= (\y -> Just (show x ++ y)))
foo :: Maybe String  
foo = do  
    x <- Just 3  
    y <- Just "!"  
    Just (show x ++ y) 

comblist :: [Int] -> [Int] -> [(Int, Int)]
comblist l1 l2= do
   x <- l1 
   y <- l2 
   return (x,y)
-- type the following in the interpreter 
-- comblist [1,2,3] [4,5]


wopwop :: Maybe Char  
wopwop = do  
    (x:xs) <- Nothing --Just ""  
    return x  
