module DataStructures where


data Triple a b c = Triple a b c
  deriving (Eq, Ord, Show, Read)

tripleFst (Triple x y z) = x
tripleSnd (Triple x y z) = y
tripleThr (Triple x y z) = z

data Quadruple a b  = Quadruple a a b b
  deriving (Eq, Ord, Show, Read)

firstTwo:: Quadruple a b  -> [a]
firstTwo (Quadruple x y z k) = x:y:[]

lastTwo:: Quadruple a b -> [b]
lastTwo (Quadruple x y z k) = z:k:[]

data List a = Nil
            | Cons a (List a)
  deriving (Eq, Ord, Show, Read)

listHead (Cons x xs) = x

listFoldl f y Nil = y 
listFoldl f y (Cons x xs) = x `f` (listFoldl f y xs)





data Tuple a b c d = TSingle a 
                     | TPair a b 
                     | TTriple a b c
                     | TQuadruple a b c d


tuple1 (TSingle x) = Just x
tuple1 (TPair x y) = Just x
tuple1 (TTriple x y z) = Just x
tuple1 (TQuadruple x y z k) = Just x

tuple4:: (Tuple a b c d) -> Maybe d 
tuple4 tuple = case tuple of
                 (TQuadruple x y z k) -> Just k
                 _ -> Nothing


retHaskellTuple:: (Tuple a b c d) -> Either (Either a (a,b)) (Either (a,b,c) (a,b,c,d))
retHaskellTuple tuple = case tuple of
                          TSingle x -> Left (Left x)
                          TPair x y -> Left (Right (x,y))
                          TTriple x y z -> Right (Left (x,y,z))
                          TQuadruple x y z k -> Right (Right (x,y,z,k))


