module Colors where 

data SomeColors
    = Red
    | Orange
--    | Yellow
--    | Green
--    | Blue
--    | Purple
--    | White
--    | Black
    | Custom Int Int Int
--    | Custom { rNumber :: Int 
--             , gNumber :: Int 
--             , bNumber :: Int
--             }
  deriving (Show) 

-- Pattern Matching 
fColor :: SomeColors -> (Int, Int, Int)
fColor Red = (255, 0, 0) 
fColor Orange = (255,128,0)
fColor (Custom a b c) = (a,b,c)

instance Eq SomeColors where
     Red == Red = True
     Orange == Orange = True
     (Custom r g b) == (Custom r' g' b') = r == r' && g == g' && b == b'
     _ == _ = False

-- Try (Custom 1 2 3) /= (Custom 1 2 2) 

data AnyColor = CustomColor 
               { rNumber :: Int 
               , gNumber :: Int 
               , bNumber :: Int
               }
  deriving (Show, Eq)

mycolor :: AnyColor
mycolor = CustomColor { rNumber = 0, gNumber = 0, bNumber = 255}

