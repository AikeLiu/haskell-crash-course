module BasicSyntax where 

list1 :: [Int]
list1 = [1, 2]

-- For loop 
list2 :: Int -> [Int]
list2 n = go [] n 
  where 
    go xs 0 = xs
    go xs k = (k * k) : (go xs (k-1)) 

-- Show standard list manipulations 
-- head, tail, take 

myMap :: ( a -> b ) -> [a] -> [b] 
myMap f (x : xs) = (f x) : (map f xs)
myMap f [] = [] 

f1 :: Int -> Int
f1 a = a + 1

g1 :: Int -> Int 
g1 a = 2 * a 

-- Show function composition
-- (f1.g1) 5, (f1.g1) $ 5, f1 $ g1 5 


