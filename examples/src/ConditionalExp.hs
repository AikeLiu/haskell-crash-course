module ConditionalExp where

-- if/then/else
absNum :: (Num a, Ord a) => a -> a 
absNum x =
     if x < 0
       then (- x) 
       else x 

-- Cases 
-- order matters 
someMap :: Int -> Int
someMap x= case x of 
       0 -> 1
       1 -> 5
       2 -> 2
       _ -> -1

-- let 
squarePlusOne :: (Num a) => a -> a 
squarePlusOne x = 
  let x2 = x * x 
  in x2 + 1

-- where 
anotherSquare :: (Num a) => a -> a  
anotherSquare x = x2 + 1
  where x2 = x*x

-- Guards 
-- must be exhaustive
myComparison :: (Ord a) => a -> a -> String
myComparison x y | x < y = "The first is less"
                 | x > y = "The second is less"
                 | otherwise = "They are equal"

 
